#include <gtest/gtest.h>
#include <jbd/overflowing>

#include <limits>
#include <stdexcept>
#include <type_traits>

using namespace jbd::overflowing;

using integers = ::testing::Types<u8,
                                  u16,
                                  u32,
                                  u64,
                                  u8f,
                                  u16f,
                                  u32f,
                                  u64f,
                                  usize,
                                  umax,
                                  i8,
                                  i16,
                                  i32,
                                  i64,
                                  i8f,
                                  i16f,
                                  i32f,
                                  i64f,
                                  isize,
                                  imax>;

template <typename T>
class OverflowingTest: public testing::Test {
};

TYPED_TEST_SUITE(OverflowingTest, integers);

TYPED_TEST(OverflowingTest, isSpecialized)
{
  using T = TypeParam;

  static_assert(std::numeric_limits<T>::is_specialized);
}

TYPED_TEST(OverflowingTest, isModulo)
{
  using T = TypeParam;

  static_assert(!std::numeric_limits<T>::is_modulo);
}

TYPED_TEST(OverflowingTest, equality)
{
  using T = TypeParam;

  static_assert(T{1} == T{1});
  static_assert(noexcept(T{1} == T{1}));
}

TYPED_TEST(OverflowingTest, inequality)
{
  using T = TypeParam;

  static_assert(T{1} != T{2});
  static_assert(noexcept(T{1} != T{1}));
}

TYPED_TEST(OverflowingTest, lesser)
{
  using T = TypeParam;

  static_assert(T{1} < T{2});
  static_assert(noexcept(T{1} < T{1}));
}

TYPED_TEST(OverflowingTest, lesserOrEqual)
{
  using T = TypeParam;

  static_assert(T{1} <= T{2});
  static_assert(T{1} <= T{1});
  static_assert(noexcept(T{1} <= T{1}));
}

TYPED_TEST(OverflowingTest, greater)
{
  using T = TypeParam;

  static_assert(T{2} > T{1});
  static_assert(noexcept(T{1} > T{1}));
}

TYPED_TEST(OverflowingTest, greaterOrEqual)
{
  using T = TypeParam;

  static_assert(T{2} >= T{1});
  static_assert(T{1} >= T{1});
  static_assert(noexcept(T{1} >= T{1}));
}

TYPED_TEST(OverflowingTest, addition)
{
  using T = TypeParam;

  static_assert(T{1} + T{1} == T{2});
  static_assert(!noexcept(T{1} + T{1}));
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(result += T{1}));
    result += T{1};
    return result;
  }() == T{2});
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(++result));
    return ++result;
  }() == T{2});
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(result++));
    return result++;
  }() == T{1});
}

TYPED_TEST(OverflowingTest, substraction)
{
  using T = TypeParam;

  static_assert(T{1} - T{1} == T{0});
  static_assert(!noexcept(T{1} - T{1}));
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(result -= T{1}));
    result -= T{1};
    return result;
  }() == T{0});
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(--result));
    return --result;
  }() == T{0});
  static_assert([]() {
    auto result = T{1};
    static_assert(!noexcept(result--));
    return result--;
  }() == T{1});
}

TYPED_TEST(OverflowingTest, multiplication)
{
  using T = TypeParam;

  static_assert(T{2} * T{3} == T{6});
  static_assert(!noexcept(T{1} * T{1}));
  static_assert([]() {
    auto result = T{2};
    static_assert(!noexcept(result *= T{3}));
    result *= T{3};
    return result;
  }() == T{6});
}

TYPED_TEST(OverflowingTest, division)
{
  using T = TypeParam;

  static_assert(T{6} / T{3} == T{2});
  static_assert(!noexcept(T{1} / T{1}));
}

TYPED_TEST(OverflowingTest, remaining)
{
  using T = TypeParam;

  static_assert(T{6} % T{3} == T{0});
  static_assert(!noexcept(T{1} % T{1}));
  static_assert([]() {
    auto result = T{6};
    static_assert(!noexcept(result %= T{3}));
    result %= T{3};
    return result;
  }() == T{0});
}

TYPED_TEST(OverflowingTest, complexDivision)
{
  using T = TypeParam;

  static_assert(T{1} / T{3} * T{3} == T{1});
}

TYPED_TEST(OverflowingTest, testAdditionOverflow)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = std::numeric_limits<T>::max() + T{1},
               std::overflow_error);
}

TYPED_TEST(OverflowingTest, testSubstractionOverflow)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = std::numeric_limits<T>::min() - T{1},
               std::overflow_error);
  ASSERT_THROW([[maybe_unused]] auto result = std::numeric_limits<T>::lowest() - T{1},
               std::overflow_error);
}

TYPED_TEST(OverflowingTest, testMultiplicationOverflow)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = std::numeric_limits<T>::max() * T{2},
               std::overflow_error);
}

TYPED_TEST(OverflowingTest, testDivisionByZero)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = T{1} / T{0}, std::invalid_argument);
}

TYPED_TEST(OverflowingTest, testRemainingByZero)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = T{1} % T{0}, std::invalid_argument);
}
