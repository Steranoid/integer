#include <gtest/gtest.h>
#include <jbd/wrapping>

#include <limits>
#include <stdexcept>
#include <type_traits>

using namespace jbd::wrapping;

using integers = ::testing::Types<u8,
                                  u16,
                                  u32,
                                  u64,
                                  u8f,
                                  u16f,
                                  u32f,
                                  u64f,
                                  usize,
                                  umax,
                                  i8,
                                  i16,
                                  i32,
                                  i64,
                                  i8f,
                                  i16f,
                                  i32f,
                                  i64f,
                                  isize,
                                  imax>;

template <typename T>
class WrappingTest: public testing::Test {
};

TYPED_TEST_SUITE(WrappingTest, integers);

TYPED_TEST(WrappingTest, isSpecialized)
{
  using T = TypeParam;

  static_assert(std::numeric_limits<T>::is_specialized);
}

TYPED_TEST(WrappingTest, isModulo)
{
  using T = TypeParam;

  static_assert(std::numeric_limits<T>::is_modulo);
}
TYPED_TEST(WrappingTest, min)
{
  using T = TypeParam;

  static_assert(std::is_same_v<T, decltype(std::numeric_limits<T>::min())>);
}


TYPED_TEST(WrappingTest, equality)
{
  using T = TypeParam;

  static_assert(T{1} == T{1});
  static_assert(noexcept(T{1} == T{1}));
}

TYPED_TEST(WrappingTest, inequality)
{
  using T = TypeParam;

  static_assert(T{1} != T{2});
  static_assert(noexcept(T{1} != T{1}));
}

TYPED_TEST(WrappingTest, lesser)
{
  using T = TypeParam;

  static_assert(T{1} < T{2});
  static_assert(noexcept(T{1} < T{1}));
}

TYPED_TEST(WrappingTest, lesserOrEqual)
{
  using T = TypeParam;

  static_assert(T{1} <= T{2});
  static_assert(T{1} <= T{1});
  static_assert(noexcept(T{1} <= T{1}));
}

TYPED_TEST(WrappingTest, greater)
{
  using T = TypeParam;

  static_assert(T{2} > T{1});
  static_assert(noexcept(T{1} > T{1}));
}

TYPED_TEST(WrappingTest, greaterOrEqual)
{
  using T = TypeParam;

  static_assert(T{2} >= T{1});
  static_assert(T{1} >= T{1});
  static_assert(noexcept(T{1} >= T{1}));
}

TYPED_TEST(WrappingTest, addition)
{
  using T = TypeParam;

  static_assert(T{1} + T{1} == T{2});
  static_assert(noexcept(T{1} + T{1}));
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(result += T{1}));
    result += T{1};
    return result;
  }() == T{2});
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(++result));
    return ++result;
  }() == T{2});
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(result++));
    return result++;
  }() == T{1});
  static_assert((std::numeric_limits<T>::max() + T{1}) == std::numeric_limits<T>::min());
}

TYPED_TEST(WrappingTest, substraction)
{
  using T = TypeParam;

  static_assert(T{1} - T{1} == T{0});
  static_assert(noexcept(T{1} - T{1}));
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(result -= T{1}));
    result -= T{1};
    return result;
  }() == T{0});
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(--result));
    return --result;
  }() == T{0});
  static_assert([]() {
    auto result = T{1};
    static_assert(noexcept(result--));
    return result--;
  }() == T{1});
  static_assert(std::numeric_limits<T>::min() - T{1} == std::numeric_limits<T>::max());
  static_assert(std::numeric_limits<T>::lowest() - T{1} == std::numeric_limits<T>::max());
}

TYPED_TEST(WrappingTest, multiplication)
{
  using T = TypeParam;

  static_assert(T{2} * T{3} == T{6});
  static_assert(noexcept(T{1} * T{1}));
  static_assert([]() {
    auto result = T{2};
    static_assert(noexcept(result *= T{3}));
    result *= T{3};
    return result;
  }() == T{6});
  static_assert(((static_cast<T>(std::numeric_limits<T>::max() / T{2}) + T{1}) * T{2}) == std::numeric_limits<T>::min());
}

TYPED_TEST(WrappingTest, division)
{
  using T = TypeParam;

  static_assert(T{6} / T{3} == T{2});
  static_assert(!noexcept(T{1} / T{1}));
  static_assert(T{6} / T{3} + T{1} == T{3});
  static_assert(T{6} / T{3} * T{2} + T{1} == T{5});
}

TYPED_TEST(WrappingTest, remaining)
{
  using T = TypeParam;

  static_assert(T{6} % T{3} == T{0});
  static_assert(!noexcept(T{1} % T{1}));
  static_assert([]() {
    auto result = T{6};
    static_assert(!noexcept(result %= T{3}));
    result %= T{3};
    return result;
  }() == T{0});
}

TYPED_TEST(WrappingTest, complexDivision)
{
  using T = TypeParam;

  static_assert(T{1} / T{3} * T{3} == T{1});
}

TYPED_TEST(WrappingTest, testDivisionByZero)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = T{1} / T{0}, std::invalid_argument);
}

TYPED_TEST(WrappingTest, testRemainingByZero)
{
  using T = TypeParam;

  ASSERT_THROW([[maybe_unused]] auto result = T{1} % T{0}, std::invalid_argument);
}
