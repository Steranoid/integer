#pragma once

#include "jbd/bits/type.hpp"

#include <limits>
#include <stdexcept>

namespace jbd::saturating {
  template <typename T>
  class integer_traits {
  public:
    using type = T;

    static constexpr auto add(type left, type right) noexcept -> type
    {
      auto result = type{};
      if (__builtin_add_overflow(left, right, &result)) {
        if (left < type{0} && right < type{0}) { return std::numeric_limits<type>::lowest(); }
        return std::numeric_limits<type>::max();
      }
      return result;
    }
    static constexpr auto add_is_noexcept = noexcept(add(type{}, type{}));

    static constexpr auto sub(type left, type right) noexcept -> type
    {
      auto result = type{};
      if (__builtin_sub_overflow(left, right, &result)) {
        if (left < type{0} && right < type{0}) { return std::numeric_limits<type>::max(); }
        return std::numeric_limits<type>::lowest();
      }
      return result;
    }
    static constexpr auto sub_is_noexcept = noexcept(sub(type{}, type{}));

    static constexpr auto mul(type left, type right) noexcept -> type
    {
      auto result = type{};
      if (__builtin_mul_overflow(left, right, &result)) {
        if ((left < type{0} && right < type{0}) || (left > type{0} && right > type{0})) {
          return std::numeric_limits<type>::max();
        }
        return std::numeric_limits<type>::lowest();
      }
      return result;
    }
    static constexpr auto mul_is_noexcept = noexcept(mul(type{}, type{}));

    static constexpr auto div(type left, type right) -> type
    {
      if (right == type{0}) { throw std::invalid_argument{"division by zero is not defined"}; }
      return left / right;
    }
    static constexpr auto div_is_noexcept = noexcept(div(type{}, type{}));

    static constexpr auto rem(type left, type right) -> type
    {
      if (right == type{0}) { throw std::invalid_argument{"division by zero is not defined"}; }
      return left % right;
    }
    static constexpr auto rem_is_noexcept = noexcept(rem(type{}, type{}));

    static constexpr auto is_modulo = false;
  };
} // namespace jbd::saturating

DECLARE_INTEGER(jbd::saturating, u8, std::uint_least8_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u16, std::uint_least16_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u32, std::uint_least32_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u64, std::uint_least64_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u8f, std::uint_fast8_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u16f, std::uint_fast16_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u32f, std::uint_fast32_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, u64f, std::uint_fast64_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, umax, std::uintmax_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, usize, std::size_t, jbd::saturating::integer_traits);

DECLARE_INTEGER(jbd::saturating, i8, std::int_least8_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i16, std::int_least16_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i32, std::int_least32_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i64, std::int_least64_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i8f, std::int_fast8_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i16f, std::int_fast16_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i32f, std::int_fast32_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, i64f, std::int_fast64_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, imax, std::intmax_t, jbd::saturating::integer_traits);
DECLARE_INTEGER(jbd::saturating, isize, ssize_t, jbd::saturating::integer_traits);
