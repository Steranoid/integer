#pragma once

#include "jbd/bits/type.hpp"

#include <stdexcept>

namespace jbd::wrapping {
  template <typename T>
  class integer_traits {
  public:
    using type = T;

    static constexpr auto add(type left, type right) noexcept -> type
    {
      auto result = type{};
      __builtin_add_overflow(left, right, &result);
      return result;
    }
    static constexpr auto add_is_noexcept = noexcept(add(type{}, type{}));

    static constexpr auto sub(type left, type right) noexcept -> type
    {
      auto result = type{};
      __builtin_sub_overflow(left, right, &result);
      return result;
    }
    static constexpr auto sub_is_noexcept = noexcept(sub(type{}, type{}));

    static constexpr auto mul(type left, type right) noexcept -> type
    {
      auto result = type{};
      __builtin_mul_overflow(left, right, &result);
      return result;
    }
    static constexpr auto mul_is_noexcept = noexcept(mul(type{}, type{}));

    static constexpr auto div(type left, type right) -> type
    {
      if (right == type{0}) { throw std::invalid_argument{"division by zero is not defined"}; }
      return left / right;
    }
    static constexpr auto div_is_noexcept = noexcept(div(type{}, type{}));

    static constexpr auto rem(type left, type right) -> type
    {
      if (right == type{0}) { throw std::invalid_argument{"division by zero is not defined"}; }
      return left % right;
    }
    static constexpr auto rem_is_noexcept = noexcept(rem(type{}, type{}));

    static constexpr auto is_modulo = true;
  };
} // namespace jbd::wrapping

DECLARE_INTEGER(jbd::wrapping, u8, std::uint_least8_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u16, std::uint_least16_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u32, std::uint_least32_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u64, std::uint_least64_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u8f, std::uint_fast8_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u16f, std::uint_fast16_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u32f, std::uint_fast32_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, u64f, std::uint_fast64_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, umax, std::uintmax_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, usize, std::size_t, jbd::wrapping::integer_traits);

DECLARE_INTEGER(jbd::wrapping, i8, std::int_least8_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i16, std::int_least16_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i32, std::int_least32_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i64, std::int_least64_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i8f, std::int_fast8_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i16f, std::int_fast16_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i32f, std::int_fast32_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, i64f, std::int_fast64_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, imax, std::intmax_t, jbd::wrapping::integer_traits);
DECLARE_INTEGER(jbd::wrapping, isize, ssize_t, jbd::wrapping::integer_traits);
