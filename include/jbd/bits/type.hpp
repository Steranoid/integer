#pragma once

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <type_traits>

#define DECLARE_INTEGER(ns, name, underlying, traits)                                            \
  namespace ns {                                                                                 \
    enum class name : underlying {                                                               \
    };                                                                                           \
    inline namespace literals {                                                                  \
      [[nodiscard]] constexpr auto operator""_##name(unsigned long long value) -> name           \
      {                                                                                          \
        return static_cast<name>(value);                                                         \
      }                                                                                          \
    }                                                                                            \
    [[nodiscard]] constexpr auto                                                                 \
    operator+(name left, name right) noexcept(traits<underlying>::add_is_noexcept) -> name       \
    {                                                                                            \
      return name{                                                                               \
        traits<underlying>::add(static_cast<underlying>(left), static_cast<underlying>(right))}; \
    }                                                                                            \
    constexpr auto operator+=(name& left, name right) noexcept(noexcept(left + right)) -> name&  \
    {                                                                                            \
      return left = left + right;                                                                \
    }                                                                                            \
    constexpr auto operator++(name& value) noexcept(noexcept(value += name{1})) -> name&         \
    {                                                                                            \
      return value += name{1};                                                                   \
    }                                                                                            \
    constexpr auto operator++(name& value, int) noexcept(noexcept(++value)) -> name              \
    {                                                                                            \
      auto result = value;                                                                       \
      ++value;                                                                                   \
      return result;                                                                             \
    }                                                                                            \
    [[nodiscard]] constexpr auto                                                                 \
    operator-(name left, name right) noexcept(traits<underlying>::sub_is_noexcept) -> name       \
    {                                                                                            \
      return name{                                                                               \
        traits<underlying>::sub(static_cast<underlying>(left), static_cast<underlying>(right))}; \
    }                                                                                            \
    constexpr auto operator-=(name& left, name right) noexcept(noexcept(left - right)) -> name&  \
    {                                                                                            \
      return left = left - right;                                                                \
    }                                                                                            \
    constexpr auto operator--(name& value) noexcept(noexcept(value -= name{1})) -> name&         \
    {                                                                                            \
      return value -= name{1};                                                                   \
    }                                                                                            \
    constexpr auto operator--(name& value, int) noexcept(noexcept(--value)) -> name              \
    {                                                                                            \
      auto result = value;                                                                       \
      --value;                                                                                   \
      return result;                                                                             \
    }                                                                                            \
    [[nodiscard]] constexpr auto                                                                 \
    operator*(name left, name right) noexcept(traits<underlying>::mul_is_noexcept) -> name       \
    {                                                                                            \
      return name{                                                                               \
        traits<underlying>::mul(static_cast<underlying>(left), static_cast<underlying>(right))}; \
    }                                                                                            \
    constexpr auto operator*=(name& left, name right) noexcept(noexcept(left * right)) -> name&  \
    {                                                                                            \
      return left = left * right;                                                                \
    }                                                                                            \
    [[nodiscard]] constexpr auto                                                                 \
    operator/(name left, name right) noexcept(traits<underlying>::div_is_noexcept)               \
      -> jbd::ratio<name, traits<underlying>>                                                    \
    {                                                                                            \
      return jbd::ratio<name, traits<underlying>>{left, right};                                  \
    }                                                                                            \
                                                                                                 \
    [[nodiscard]] constexpr auto                                                                 \
    operator%(name left, name right) noexcept(traits<underlying>::rem_is_noexcept) -> name       \
    {                                                                                            \
      return name{                                                                               \
        traits<underlying>::rem(static_cast<underlying>(left), static_cast<underlying>(right))}; \
    }                                                                                            \
    constexpr auto operator%=(name& left, name right) noexcept(noexcept(left % right)) -> name&  \
    {                                                                                            \
      return left = left % right;                                                                \
    }                                                                                            \
  }                                                                                              \
                                                                                                 \
  namespace std {                                                                                \
    template <>                                                                                  \
    struct numeric_limits<ns::name> {                                                            \
      static constexpr auto is_specialized = true;                                               \
      static constexpr auto is_signed      = std::numeric_limits<underlying>::is_signed;         \
      static constexpr auto is_integer     = std::numeric_limits<underlying>::is_integer;        \
      static constexpr auto is_exact       = std::numeric_limits<underlying>::is_exact;          \
      static constexpr auto has_inifinity  = std::numeric_limits<underlying>::has_infinity;      \
      static constexpr auto has_quiet_NaN  = std::numeric_limits<underlying>::has_quiet_NaN;     \
      static constexpr auto has_signaling_NaN =                                                  \
        std::numeric_limits<underlying>::has_signaling_NaN;                                      \
      static constexpr auto has_denorm      = std::numeric_limits<underlying>::has_denorm;       \
      static constexpr auto has_denorm_loss = std::numeric_limits<underlying>::has_denorm_loss;  \
      static constexpr auto round_style     = std::numeric_limits<underlying>::round_style;      \
      static constexpr auto is_iec559       = std::numeric_limits<underlying>::is_iec559;        \
      static constexpr auto is_bounded      = std::numeric_limits<underlying>::is_bounded;       \
      static constexpr auto is_modulo       = traits<ns::name>::is_modulo;                       \
      static constexpr auto is_digits       = std::numeric_limits<underlying>::digits;           \
      static constexpr auto is_digits10     = std::numeric_limits<underlying>::digits10;         \
      static constexpr auto max_digits10    = std::numeric_limits<underlying>::max_digits10;     \
      static constexpr auto radix           = std::numeric_limits<underlying>::radix;            \
      static constexpr auto min_exponent    = std::numeric_limits<underlying>::min_exponent;     \
      static constexpr auto min_exponent10  = std::numeric_limits<underlying>::min_exponent10;   \
      static constexpr auto max_exponent    = std::numeric_limits<underlying>::max_exponent;     \
      static constexpr auto max_exponent10  = std::numeric_limits<underlying>::max_exponent10;   \
      static constexpr auto traps           = std::numeric_limits<underlying>::traps;            \
      static constexpr auto tinyness_before = std::numeric_limits<underlying>::tinyness_before;  \
                                                                                                 \
      [[nodiscard]] static constexpr auto max() noexcept                                         \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::max()};                                 \
      }                                                                                          \
      [[nodiscard]] static constexpr auto min() noexcept                                         \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::min()};                                 \
      }                                                                                          \
      [[nodiscard]] static constexpr auto lowest() noexcept                                      \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::lowest()};                              \
      }                                                                                          \
      [[nodiscard]] static constexpr auto epsilon() noexcept                                     \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::epsilon()};                             \
      }                                                                                          \
      [[nodiscard]] static constexpr auto round_error() noexcept                                 \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::round_error()};                         \
      }                                                                                          \
      [[nodiscard]] static constexpr auto quiet_NaN() noexcept                                   \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::quiet_NaN()};                           \
      }                                                                                          \
      [[nodiscard]] static constexpr auto signaling_NaN() noexcept                               \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::signaling_NaN()};                       \
      }                                                                                          \
      [[nodiscard]] static constexpr auto denorm_min() noexcept                                  \
      {                                                                                          \
        return ns::name{std::numeric_limits<underlying>::denorm_min()};                          \
      }                                                                                          \
    };                                                                                           \
  }

namespace jbd {
  template <typename T, typename traits>
  class ratio {
  public:
    using type       = T;
    using underlying = std::underlying_type_t<type>;

    constexpr ratio(type numerator, type denominator = type{1})
      : numerator{numerator}, denominator{denominator}
    {
      [[maybe_unused]] auto result =
        traits::div(static_cast<underlying>(numerator), static_cast<underlying>(denominator));
    }

    [[nodiscard]] friend constexpr auto operator+(ratio left, ratio right) -> ratio
    {
      return ratio{left.numerator * right.denominator + right.numerator * left.denominator,
                   left.denominator * right.denominator};
    }
    friend constexpr auto operator+=(ratio& left, ratio right) -> ratio&
    {
      return left = left + right;
    }
    friend constexpr auto operator++(ratio& left) -> ratio&
    {
      return left += type{1};
    }
    friend constexpr auto operator++(ratio& left, int) -> ratio&
    {
      auto result = left;
      ++left;
      return result;
    }
    [[nodiscard]] friend constexpr auto operator-(ratio value) -> ratio
    {
      return ratio{-value.numerator, value.denominator};
    }
    [[nodiscard]] friend constexpr auto operator-(ratio left, ratio right) -> ratio
    {
      return ratio{left.numerator * right.denominator - right.numerator * left.denominator,
                   left.denominator * right.denominator};
    }
    friend constexpr auto operator-=(ratio& left, ratio right) -> ratio&
    {
      return left = left - right;
    }
    friend constexpr auto operator--(ratio& left) -> ratio&
    {
      return left -= type{1};
    }
    friend constexpr auto operator--(ratio& left, int) -> ratio&
    {
      auto result = left;
      --left;
      return result;
    }
    [[nodiscard]] friend constexpr auto operator*(ratio left, ratio right) -> ratio
    {
      return ratio{left.numerator * right.numerator, left.denominator * right.denominator};
    }
    friend constexpr auto operator*=(ratio& left, ratio right) -> ratio&
    {
      return left = left * right;
    }
    [[nodiscard]] friend constexpr auto operator/(ratio left, ratio right) -> ratio
    {
      return ratio{left.numerator * right.denominator, left.denominator * right.numerator};
    }
    friend constexpr auto operator/=(ratio& left, ratio right) -> ratio&
    {
      return left = left / right;
    }

    [[nodiscard]] friend constexpr auto operator==(ratio left, ratio right) -> bool
    {
      return left.numerator * right.denominator == right.numerator * left.denominator;
    }
    [[nodiscard]] friend constexpr auto operator!=(ratio left, ratio right) -> bool
    {
      return !(left == right);
    }
    [[nodiscard]] friend constexpr auto operator<(ratio left, ratio right) -> bool
    {
      return left.numerator * right.denominator < right.numerator * left.denominator;
    }
    [[nodiscard]] friend constexpr auto operator<=(ratio left, ratio right) -> bool
    {
      return left.numerator * right.denominator <= right.numerator * left.denominator;
    }
    [[nodiscard]] friend constexpr auto operator>(ratio left, ratio right) -> bool
    {
      return !(left <= right);
    }
    [[nodiscard]] friend constexpr auto operator>=(ratio left, ratio right) -> bool
    {
      return !(left < right);
    }

    [[nodiscard]] explicit constexpr operator type() const
    {
      return type{
        traits::div(static_cast<underlying>(numerator), static_cast<underlying>(denominator))};
    }

  private:
    type numerator;
    type denominator;
  };
} // namespace jbd
